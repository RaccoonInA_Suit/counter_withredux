import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';

const Counter = ({counter, inc, dec, res}) => {
    return (
        <div className="content">
        <div className="counter-wrapper">
            <h1 id="counter">{counter}</h1>
        </div>
        <div className="btn-wrapper">
          <button onClick = {inc} className = "btn btn-inc">
            <img src="images/Plus.svg" alt="logo"/>
          </button>
          <button onClick = {dec} className = "btn btn-dec">
            <img src="images/Minus.svg" alt="logo"/>
          </button>
          <button onClick = {res} className = "btn btn-res">
            <img src="images/Vector-top.svg" alt="logo"/>
            <img src="images/Vector-down.svg" alt="logo"/>
          </button>
        </div>
      </div> 
    );
}

const mapStateToProps = (state) => {
  return {
    counter: state
  }
}

export default connect(mapStateToProps, actions)(Counter);